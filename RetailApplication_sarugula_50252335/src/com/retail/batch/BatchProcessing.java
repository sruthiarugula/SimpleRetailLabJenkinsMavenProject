package com.retail.batch;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.retail.core.Item;
import com.retail.core.ItemComparator;
import com.retail.report.Report;

public class BatchProcessing {

public void	generateBatch(Set<Item> set){
	List<Item> list = new ArrayList<Item>(set);
	Report report = new Report();
	int k=6;

int chunks=	list.size()/6;

for(int i=0; i<chunks;i++){
	if(i==0){
		
		Set<Item> subset=new TreeSet<Item>(new ItemComparator());
		subset.addAll(list.subList(i, 6*i+k));
		report.getAllItems(subset);
		
			}
	 if(i>0 && i<chunks){
		
		Set<Item> subset2=new TreeSet<Item>(new ItemComparator());
		 
		subset2.addAll(list.subList(6*i, 6*i+k));
		report.getAllItems(subset2);
		
		 subset2.removeAll(subset2);
	}
	
}
	
	
		
	

}}

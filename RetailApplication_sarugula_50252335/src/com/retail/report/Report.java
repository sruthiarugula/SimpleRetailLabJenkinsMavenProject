package com.retail.report;

import java.sql.Timestamp;
import java.util.Set;

import com.retail.core.Item;

public class Report {
	
	public  void getAllItems(Set<Item> set) {
		int totalCost=0;
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("***********SHIPMENT REPORT**********                   " + new Timestamp(System.currentTimeMillis()));
	    System.out.printf("%30s %50s %20s %5s %10s %10s", "UPC", "Descritpion", "Price", "Weight", "Shipping Type","Shipping Cost");
	    System.out.println();
	    System.out.println("-------------------------------------------------------------------------------------------------------------------");
	    for(Item item: set){
	        System.out.format("%10s %50s %20s %5s %10s %10s", 
	                item.getUPC(), item.getDescription(), item.getPrice(), item.getWeight(), item.getShipMethod(),item.getShippingCost());
	        System.out.println();
	        totalCost+=item.getShippingCost();
	    }
	    System.out.println();
	    System.out.println("TOTAL COST                                                                                                                 " + totalCost);
	    System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
	   
	}

}

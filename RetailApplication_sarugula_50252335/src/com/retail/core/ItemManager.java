package com.retail.core;

import java.util.Set;

import java.util.TreeSet;

import com.retail.batch.BatchProcessing;
import com.retail.shipping.IShippingCost;
import com.retail.shipping.ShipCost;
import com.retail.shipping.ShippingFactory;

class ItemManager {
	static Set<Item> set = new TreeSet<Item>(new ItemComparator());

	public static void main(String[] args) {
		// Creating an object of database
		ItemSupplier itemSupplier = new ItemSupplier();
		// validating the length of UPC
		for (Item item : itemSupplier.SupplyItems()) {

			if (item.UPCLength() == 12) {
				set.add(item);
			}
		}
		// calculating the shipcost
		calculateShipCost(set);
		// breaking the batches into 6 per chunks
		batchProcessing(set);

		// getAllItems(set);
	}

	public static void getAllItems(Set<Item> set) {
		System.out.println(
				"******************************************************************************************************************");
		System.out.printf("%10s %40s %20s %5s %10s %10s", "UPC", "Descritpion", "Price", "Weight", "Shipping Type",
				"Shipping Cost");
		System.out.println();
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------");
		for (Item item : set) {
			System.out.format("%10s %40s %20s %5s %10s %10s", item.getUPC(), item.getDescription(), item.getPrice(),
					item.getWeight(), item.getShipMethod(), item.getShippingCost());
			System.out.println();
			System.out.println(
					"******************************************************************************************************************");

		}
		System.out.println(
				"----------------------------------------------------------------------------------------------------------------------");
		System.out.println(
				"******************************************************************************************************************");
	}
//Implementing Factory Design Pattern
	static Set<Item> calculateShipCost(Set<Item> set) {
		// Delegate to ShipManager
		ShipCost sc= new ShipCost();
		return sc.calculateShipCost(set);
	}

	static void batchProcessing(Set<Item> set) {

		BatchProcessing batchProcessing = new BatchProcessing();
		batchProcessing.generateBatch(set);

	}

}

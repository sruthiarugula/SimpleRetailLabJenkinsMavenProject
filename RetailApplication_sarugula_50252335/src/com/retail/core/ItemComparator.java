package com.retail.core;

import java.util.Comparator;

public class ItemComparator implements Comparator<Item>{
	 public int compare(Item e1, Item e2) {
	    int upcCompare= Long.compare(e1.getUPC(),e2.getUPC());
	    
	   return upcCompare;
}
}


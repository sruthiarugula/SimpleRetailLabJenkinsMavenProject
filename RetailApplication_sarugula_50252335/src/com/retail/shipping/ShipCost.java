package com.retail.shipping;

import java.util.Set;

import com.retail.core.Item;

public class ShipCost {
	
	public Set<Item> calculateShipCost(Set<Item> set) {
		// Delegate to ShipManager
		ShippingFactory sm = new ShippingFactory();
		for (Item item : set) {
			IShippingCost ishippingcost = sm.calculateShipCost(item);
			item.setShippingCost(ishippingcost.shippingCost(item));
		}
		return set;
	}


}

package com.retail.shipping;

import com.retail.core.Item;

public class RailShipping  implements IShippingCost{
	public double shippingCost(Item item) {
		// TODO Auto-generated method stub

		double cost;

		if (item.getWeight() < 5) {
			return 5.0;
		} else if (item.getWeight() > 5) {
			return 10.0;
		} else {
			return 0.0;
		}

	}

}
